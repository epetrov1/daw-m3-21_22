#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define MAX_MATRICULA 8
#define MAX_MARCA 20
#define MAX_MODEL 100
#define MAX_HTML 200
#define NOM_FITXER "cotxes.dat"



typedef struct {
    char matricula[MAX_MATRICULA];
    char marca[MAX_MARCA];
    int numeroPortes;
    int potencia;
    double consum;
    bool teEtiquetaECO;
    char model[MAX_MODEL];


} Cotxe; //estructura que representa el fitxer intern.
void entrarCotxe(Cotxe *perso);
void escriureCotxe(Cotxe perso);
int alta(char nomfitxer[]);
void menu();
int opcioMenuTriada();
int llistat(char nomFitxer[]);


int main()
{
    int opcio,error;


    do{
        menu();
        opcio=opcioMenuTriada();
        switch(opcio){
        case 1:
            error=alta(NOM_FITXER);
            if(error==-1) printf("Error en obrir el fitxer");
            if(error==-2) printf("Error d'escriptura");
            break;
        case 2:
            error=llistat(NOM_FITXER);
            if(error==-1) printf("Error en obrir el fitxer");
            if(error==-3) printf("Error de lectura");
            break;
        case 0:
            //res
            break;
        case 3:
            error=generar_informe(NOM_FITXER);
            if(error==-4) printf("Error en obrir el fitxer");
            if(error==-1) printf("Error de lectura");
            break;
        default:
            printf("Les opcions valides s�n de 0 a 3");
        }
    }while(opcio!=0);

    //obrir per escriure en binari. Si existeix el matxaca sin� el crea.

    //obrir per llegir amb binari. El fitxer ha d'existir.

/*system("clear");*/
    return 0;
}

int alta(char nomfitxer[]){
    Cotxe c1;
    FILE *f1;
    char sn;
    int n;
    f1= fopen(nomfitxer,"ab");
    if( f1 == NULL ) {
        printf("Error en obrir el fitxer ");
        return -1;
    }
    do{
        entrarCotxe(&c1);
        n= fwrite(&c1,sizeof(Cotxe),1,f1);
        if(n==0) {
            printf("error d'escriptura");
            return -2;
        }
        do{
            printf("\nVols seguir entran les dades? (s/n) ");
            scanf("%c",&sn);while(getchar()!='\n');
        }while(sn!='s' && sn!='n');
    }while(sn=='s');
    fclose(f1);
    return 0;
}

void menu(){
    system("clear");
    printf("***MENU***\n1.Alta\n2.Llistat\n3.Generar Informe\n0.Sortir\n__________\n\nSelecciona una opcio (0-3): ");
}

int opcioMenuTriada(){
    int opcio;
    scanf("%d",&opcio);while(getchar()!='\n');
    return opcio;
}

void entrarCotxe(Cotxe *cotx){
    char sn;

    printf("\nIntrodueix la matr�cula: ");
    scanf("%15[^\n]",cotx->matricula);while(getchar()!='\n');


    printf("\nIntrodueix la marca: ");
    scanf("%35[^\n]",cotx->marca);while(getchar()!='\n');


    printf("\nIntrodueix el model: ");
    scanf("%100[^\n]",cotx->model);while(getchar()!='\n');


    printf("\nIntrodueix el numero de portes: ");
    scanf("%d",&cotx->numeroPortes);while(getchar()!='\n');


    printf("\nIntrodueix la pot�ncia: ");
    scanf("%d",&cotx->potencia);while(getchar()!='\n');


    printf("\nIntrodueix el consum: ");
    scanf("%lf",&cotx->consum);while(getchar()!='\n');


    do{
        printf("\nTe etiqueta ECO? (s/n)");
        scanf("%c",&sn);while(getchar()!='\n');
    }while(sn!='s' && sn!='n');
    if(sn=='s'){
        cotx->teEtiquetaECO=true;
    }else{
        cotx->teEtiquetaECO=false;
    }

}

int llistat(char nomFitxer[]){
    FILE *f2;
    int n;
    char continuar;
    Cotxe c2;
    f2= fopen(nomFitxer,"rb");
    if( f2 == NULL ) {
        printf("Error en obrir el fitxer ");
        return -1;
    }

    while(!feof(f2)){
        n = fread(&c2,sizeof(Cotxe), 1,f2);
        if(!feof(f2)){
            if(n==0) {
                printf("Error de lectura");
                return -3;
            }
            escriureCotxe(c2);
        }
    }
    fclose(f2);
    printf("Prem una tecla per continuar...");
    scanf("%c",&continuar);
    return 0;
}

void escriureCotxe(Cotxe cotx){
    printf("\nla matr�cula: %s", cotx.matricula);
    printf("\nels marca: %s",cotx.marca);
    printf("\nel model: %s",cotx.model);
    printf("\nel n�mero de portes: %d",cotx.numeroPortes);
    printf("\nla pot�ncia: %d",cotx.potencia);
    printf("\nel consum: %lf",cotx.consum);
    if(cotx.teEtiquetaECO){
        printf("S�, el teu cotxe t� etiqueta ECO");
    }else printf("No t� etiqueta ECO");
}

int generar_informe(char nomFitxer[]){
    char principi_html[MAX_HTML+1]="<!DOCTYPE html> \
        <html lang=\"es\"> \
          <head> \
            <meta charset=\"UTF-8\"> \
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> \
            <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"> \
            <title>Cotxes</title> \
            <link rel=\"stylesheet\" href=\"reset.css\"> \
            <link rel=\"stylesheet\" href=\"styles.css\"> \
          </head> \
          <body class=\"app\"> \
            <header class=\"header\"> \
                <h1>Informe De Cotxes</h1> \
            </header> \
                <table class=\"table\"> \
                    <tr class=\"table-th\"> \
                        <th>Matricula</th> \
                        <th>Marca</th> \
                        <th>Model</th> \
                        <th>Numero Portes</th> \
                        <th>Potencia</th> \
                        <th>Consum</th> \
                        <th>Etiqueta ECO</th> \
                    </tr>";
    char dades[MAX_HTML+1];
    char footer_html[MAX_HTML+1]="</table> \
            <footer class=\"footer\"> \
                Erik Petrov Borisov<br> \
                M3-UF3 \
            </footer> \
          </body> \
        </html>";
    int n;
    char textTemporal[MAX_HTML];
    dades[0]='\0';
    Cotxe c1;
    FILE *f1, *f2;
    if((f1=fopen("dades.html","w"))==NULL){
        return -4;
    }

    if((f2=fopen(nomFitxer,"r"))==NULL){
            return -1;
    }

    strcat(dades,principi_html);

    while(!feof(f2)){
        n=fread(&c1,sizeof(Cotxe),1,f2);
        if(!feof(f2)){
            if(n==0) return -3;
            strcat(dades,"<tr><td>");
            strcat(dades,c1.matricula);
            strcat(dades,"</td>");
            strcat(dades,"<td>");
            strcat(dades,c1.marca);
            strcat(dades,"</td>");
            strcat(dades,"<td>");
            strcat(dades,c1.model);
            strcat(dades,"</td>");
            sprintf(textTemporal,"%d",c1.numeroPortes);
            strcat(dades,"<td>");
            strcat(dades,textTemporal);
            strcat(dades,"</td>");
            sprintf(textTemporal,"%d",c1.potencia);
            strcat(dades,"<td>");
            strcat(dades,textTemporal);
            strcat(dades,"</td>");
            sprintf(textTemporal,"%.2lf",c1.consum);
            strcat(dades,"<td>");
            strcat(dades,textTemporal);
            strcat(dades,"</td>");
            if(c1.teEtiquetaECO) strcpy(textTemporal,"Si");
            else strcpy(textTemporal,"No");
            strcat(dades,"<td>");
            strcat(dades,textTemporal);
            strcat(dades,"</td>");
            strcat(dades,"</tr>");
        }
    }
    strcat(dades,footer_html);
    fputs(dades,f1);
    fclose(f1);
    fclose(f2);
    system("firefox dades.html");

    return 0;
}
